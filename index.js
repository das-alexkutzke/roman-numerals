#!/usr/bin/env node

const yargs = require('yargs');
const RomanNumeral = require('./roman_numeral.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <roman number>")
   .option("n", { alias: "roman-number", describe: "Roman number to convert", type: "string", demandOption: true })
   .argv;

  const rn = new RomanNumeral(options.n);
    
  console.log(rn.toInt());
}
