Especialização em Desenvolvimento Ágil de Software

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Infra-estrutura para desenvolvimento e implantação de Software (DevOps)*

Prof. Alexander Robert Kutzke

# Exemplo de app cli com nodejs

```sh
git clone https://gitlab.com/das-alexkutzke/roman-numerals
cd roman-numerals
npm install
bin/rn III
```
